# Disegni Cascine

### Ecco le versioni ad "alta risoluzione" del bellissimo disegno di **Maria Cemmi!**

## CONTRIBUTORS

This repository is mantained by [Francesco Zubani](https://fatualux.github.io), but the drawing was made by **Maria Cemmi**.
I only made the upscaled and vectorial versions.

## LICENSE

[![License](https://img.shields.io/badge/License-CC%20BY--NC--ND-green)](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.en)

This project is licensed under the CC BY-NC-ND 4.0 Deed license.
See LICENSE file for more details.

