# MAV_ValSorda - Assets

### This is the MAV's repository containing the assets for "Valsorda Project".

## CONTRIBUTORS

This repository is mantained by [Francesco Zubani](https://fatualux.github.io), but the material is produced by **the members of the MAV community**.

## LICENSE

[![License](https://img.shields.io/badge/License-CC%20BY--NC--ND-green)](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.en)

This project is licensed under the CC BY-NC-ND 4.0 Deed license.
See LICENSE file for more details.
