
## MANTAINER

- [Francesco Zubani](https://fatualux.github.io/)

## AUTHORS

- MAV Team

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [Stable Diffusion](https://github.com/CompVis/stable-diffusion) - [LICENSE](https://github.com/CompVis/stable-diffusion/blob/main/LICENSE)
- [ComfyUI](https://github.com/comfyanonymous/ComfyUI) - [LICENSE](https://github.com/comfyanonymous/ComfyUI/blob/master/LICENSE)
- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)

## LICENSE

[![License](https://img.shields.io/badge/License-CC%20BY--NC--ND-green)](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.en)

This project is licensed under the CC BY-NC-ND 4.0 Deed license.
See LICENSE file for more details.

## GALLERY

### Montagnaterapia

<div class="gallery">
  <a href="2024_draft_01_.png"><img class="thumbnail" src="./thumbs/2024_draft_01_.png" alt="2024_draft_01_"></a>
  <a href="2024_draft_02_.png"><img class="thumbnail" src="./thumbs/2024_draft_02_.png" alt="2024_draft_02_"></a>
  <a href="2024_draft_03_.png"><img class="thumbnail" src="./thumbs/2024_draft_03_.png" alt="2024_draft_03_"></a>
  <a href="2024_draft_04_.png"><img class="thumbnail" src="./thumbs/2024_draft_04_.png" alt="2024_draft_04_"></a>
  <a href="2024_draft_05_.png"><img class="thumbnail" src="./thumbs/2024_draft_05_.png" alt="2024_draft_05_"></a>
  <a href="2024_draft_06_.png"><img class="thumbnail" src="./thumbs/2024_draft_06_.png" alt="2024_draft_06_"></a>
  <a href="2024_draft_07_.png"><img class="thumbnail" src="./thumbs/2024_draft_07_.png" alt="2024_draft_07_"></a>
  <a href="2024_draft_08_.png"><img class="thumbnail" src="./thumbs/2024_draft_08_.png" alt="2024_draft_08_"></a>
  <a href="2024_draft_09_.png"><img class="thumbnail" src="./thumbs/2024_draft_09_.png" alt="2024_draft_09_"></a>
  <a href="2024_draft_10_.png"><img class="thumbnail" src="./thumbs/2024_draft_10_.png" alt="2024_draft_10_"></a>
  <a href="2024_draft_11_.png"><img class="thumbnail" src="./thumbs/2024_draft_11_.png" alt="2024_draft_11_"></a>
  <a href="2024_draft_12_.png"><img class="thumbnail" src="./thumbs/2024_draft_12_.png" alt="2024_draft_12_"></a>
  <a href="2024_draft_13_.png"><img class="thumbnail" src="./thumbs/2024_draft_13_.png" alt="2024_draft_13_"></a>
  <a href="2024_draft_14_.png"><img class="thumbnail" src="./thumbs/2024_draft_14_.png" alt="2024_draft_14_"></a>
  <a href="2024_draft_15_.png"><img class="thumbnail" src="./thumbs/2024_draft_15_.png" alt="2024_draft_15_"></a>
  <a href="2024_draft_21_.png"><img class="thumbnail" src="./thumbs/2024_draft_21_.png" alt="2024_draft_21_"></a>
  <a href="2024_draft_22_.png"><img class="thumbnail" src="./thumbs/2024_draft_22_.png" alt="2024_draft_22_"></a>
  <a href="2024_draft_23_.png"><img class="thumbnail" src="./thumbs/2024_draft_23_.png" alt="2024_draft_23_"></a>
  <a href="2024_draft_24_.png"><img class="thumbnail" src="./thumbs/2024_draft_24_.png" alt="2024_draft_24_"></a>
  <a href="2024_draft_25_.png"><img class="thumbnail" src="./thumbs/2024_draft_25_.png" alt="2024_draft_25_"></a>
  <a href="2024_draft_26_.png"><img class="thumbnail" src="./thumbs/2024_draft_26_.png" alt="2024_draft_26_"></a>
  <a href="2024_draft_27_.png"><img class="thumbnail" src="./thumbs/2024_draft_27_.png" alt="2024_draft_27_"></a>
  <a href="2024_draft_28_.png"><img class="thumbnail" src="./thumbs/2024_draft_28_.png" alt="2024_draft_28_"></a>
  <a href="2024_draft_29_.png"><img class="thumbnail" src="./thumbs/2024_draft_29_.png" alt="2024_draft_29_"></a>
  <a href="2024_draft_30_.png"><img class="thumbnail" src="./thumbs/2024_draft_30_.png" alt="2024_draft_30_"></a>
  <a href="2024_draft_31_.png"><img class="thumbnail" src="./thumbs/2024_draft_31_.png" alt="2024_draft_31_"></a>
  <a href="2024_draft_32_.png"><img class="thumbnail" src="./thumbs/2024_draft_32_.png" alt="2024_draft_32_"></a>
  <a href="2024_draft_33_.png"><img class="thumbnail" src="./thumbs/2024_draft_33_.png" alt="2024_draft_33_"></a>
  <a href="2024_draft_34_.png"><img class="thumbnail" src="./thumbs/2024_draft_34_.png" alt="2024_draft_34_"></a>
  <a href="2024_draft_35_.png"><img class="thumbnail" src="./thumbs/2024_draft_35_.png" alt="2024_draft_35_"></a>
  <a href="draft_2024.png"><img class="thumbnail" src="./thumbs/draft_2024.png" alt="draft_2024"></a>
</div>

## Image data:

```
Image Width: 960px
Image Height: 512px
Bit Depth: 8
Color Type: RGB
Compression: Deflate/Inflate
Filter: Adaptive
Interlace: Noninterlaced

prompt: {
  "10": {
    "inputs": {
      "ckpt_name": "Dreamshaper.safetensors"
    },
    "class_type": "CheckpointLoaderSimple"
  },
  "16": {
    "inputs": {
      "text": "artwork of a group of kids, male and female, holding their hands in the background a beautiful scenery of mountains and nature, flowers, trees, digital art, 8k, natural light, highresfix, work of art, masterpiece, detailed, rich composition, award winning, trending on artstation, trending on deviantart\n\n",
      "clip": ["10", 1]
    },
    "class_type": "CLIPTextEncode"
  },
  "17": {
    "inputs": {
      "text": "bw, worst quality, low quality, (deformed, distorted, disfigured:1), poorly drawn, bad anatomy, wrong anatomy, extra limb, missing limb, floating limbs, (mutated hands and fingers:1.4), disconnected limbs, mutation, mutated, (ugly), disgusting, blurry, amputation, cloned, (solo:1.4),EasyNegative, (worst quality, low quality:1.0), badhandv4,",
      "clip": ["10", 1]
    }
```
